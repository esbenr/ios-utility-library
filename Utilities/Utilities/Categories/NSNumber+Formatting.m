//
//  NSNumber+Formatting.m
//  BilBasen
//
//  Created by Esben Roloff Rasmussen on 01/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSNumber+Formatting.h"

@implementation NSNumber (ThousandSeparated)

- (NSString *)stringByAddingThousandSeparators
{
    //American number format - with comma sepparators between thousands
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setDecimalSeparator:@","];
    [numberFormatter setGroupingSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //If you print next thing
    return [numberFormatter stringFromNumber:self];
}

- (NSString *)roundToNumberOfDecimals:(int)nofDecimals
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init]; 
    [formatter setMaximumFractionDigits:nofDecimals]; 
    [formatter setRoundingMode: NSNumberFormatterRoundFloor];  //NSNumberFormatterRoundDown
    return [formatter stringFromNumber:self]; 
}

@end
