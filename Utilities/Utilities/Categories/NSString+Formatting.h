//
//  NSString+Formatting.h
//  BilBasen
//
//  Created by Esben Roloff Rasmussen on 15/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Formatting)

+ (NSString*)customStringWithFormat:(NSString*)formatString andObject:(id)object1 andObject:(id)object2 andObject:(id)object3;
+ (NSString*)customStringWithFormat:(NSString*)formatString andObject:(id)object1 andObject:(id)object2;
+ (NSString*)customStringWithFormat:(NSString*)formatString andObject:(id)object1;

+ (id)validateObject:(id)object;

@end
