#import "NSMutableString+Append.h"


@implementation NSMutableString (Append)

- (void)appendLine:(NSString *)lineToAppend
{
    if (![self hasSuffix:@"\n"] && self.length > 0)
        [self appendString:@"\n"];
    [self appendString:lineToAppend];
}

@end