//
//  NSString+Formatting.m
//  BilBasen
//
//  Created by Esben Roloff Rasmussen on 15/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Formatting.h"

@implementation NSString (Formatting)

+ (id)validateObject:(id)object
{
    if([object isKindOfClass:[NSNumber class]])
        if([(NSNumber*)object intValue] == 0)
            object = nil;
    
    if([object isKindOfClass:[NSString class]])
        if([(NSString*)object isEqualToString:@""])
            object = nil;
    
    return object;
}

+ (NSString*)customStringWithFormat:(NSString*)formatString andObject:(id)object1 andObject:(id)object2 andObject:(id)object3
{
    object1 = [self validateObject:object1];
    object2 = [self validateObject:object2];
    object3 = [self validateObject:object3];
    
    if(object1 && object2 && object3)
        return [NSString stringWithFormat:formatString, object1, object2, object3];
    
    return nil;
}

+ (NSString*)customStringWithFormat:(NSString*)formatString andObject:(id)object1 andObject:(id)object2
{
    object1 = [self validateObject:object1];
    object2 = [self validateObject:object2];
    
    if(object1 && object2)
        return [NSString stringWithFormat:formatString, object1, object2];
    
    return nil;
}

+ (NSString*)customStringWithFormat:(NSString*)formatString andObject:(id)object1
{
    object1 = [self validateObject:object1];
           
    if(object1)
        return [NSString stringWithFormat:formatString, object1];
    
    return nil;
}

//- (NSString*)customStringWithFormat:(NSString*)formatString, ... 
//{
//    NSString *returnValue = [[[NSString alloc] init] autorelease];
//        
//    va_list args;
//    va_start(args, formatString);
//    returnValue = [[NSString alloc] initWithFormat:formatString arguments:args];
//    va_end(args);
//    
//    va_list args2;
//    va_start(args2, formatString);
//    id eachObject;
//    bool wasHere = NO;
//    while (eachObject = va_arg(args2, id))
//    {
//        wasHere = YES;
//    }
//    va_end(args2);
//    
//    if(!wasHere)
//        return nil;
//    
//    return returnValue;
//}


@end
