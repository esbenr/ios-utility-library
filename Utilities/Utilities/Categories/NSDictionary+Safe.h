#import <Foundation/Foundation.h>

@interface NSDictionary (Safe)
-(id)safeObjectForKey:(NSString *)key;
@end