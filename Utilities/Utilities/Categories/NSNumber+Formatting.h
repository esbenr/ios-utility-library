//
//  NSNumber+Formatting.h
//  BilBasen
//
//  Created by Esben Roloff Rasmussen on 01/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Formatting)

- (NSString *)stringByAddingThousandSeparators;
- (NSString *)roundToNumberOfDecimals:(int)nofDecimals;

@end
