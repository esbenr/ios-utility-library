#import <Foundation/Foundation.h>

@interface NSMutableString (Append)
- (void)appendLine:(NSString *)lineToAppend;


@end