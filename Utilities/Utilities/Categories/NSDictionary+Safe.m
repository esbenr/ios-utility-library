#import "NSDictionary+Safe.h"

@implementation NSDictionary (Safe)

- (id)safeObjectForKey:(NSString *)key
{
    id obj = [self objectForKey:key];
    if (obj == (id) [NSNull null])
        return nil;
    return obj;
}

@end