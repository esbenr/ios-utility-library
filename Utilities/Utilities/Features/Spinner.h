//
//  Spinner.h
//  dba
//
//  Created by DBA on 21/06/11.
//  Copyright 2011 Den Blå Avis a/s. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Spinner : UIView {
    
}

@property (nonatomic, retain) UIViewController *delegate;

- (id)initWithDelegate:(UIViewController*)viewController;

- (void)show;
- (void)hide;

@end
