//
//  DeviceHardware.m
//  Marktplaats
//
//  Created by Gyorgy Varadi on 1/20/11.
//  Copyright 2011 Marktplaats B.V. All rights reserved.
//

#import "DeviceHardware.h"
#include <sys/types.h>
#include <sys/sysctl.h>


@implementation DeviceHardware

- (NSString *)platform {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    return platform;
}

@end
