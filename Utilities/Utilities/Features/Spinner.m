//
//  Spinner.m
//  dba
//
//  Created by DBA on 21/06/11.
//  Copyright 2011 Den Blå Avis a/s. All rights reserved.
//

#import "Spinner.h"
#import <QuartzCore/QuartzCore.h> // for CALayer + CAGradientLayer
#import "DbaConfig.h"
#import "Colors.h"

@implementation Spinner
@synthesize delegate = _delegate;


- (id)initWithFrame:(CGRect)frame
{
    float w = frame.size.width;
    float h = frame.size.height;
    float w2 = w/2.0f;
    float h2 = h/4.0f;
    self = [super initWithFrame:CGRectMake((w-w2)/2, (h-h2)/2, w2, h2)];
    if (self) {
        
        self.backgroundColor = RGBA(0,0,0,0.5);
        self.layer.cornerRadius = 5.0f;
        self.userInteractionEnabled = NO;
        self.isAccessibilityElement = YES;
        self.accessibilityLabel = @"Henter";
        UIActivityIndicatorView* s = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        float asize = h2/2.0f;
        s.frame = CGRectMake((w2 - asize)/2.0f, (h2 - asize/2.0f)/2.0f, asize, asize);
        s.center = CGPointMake(w2/2.0f, h2/2.0f);
        [self addSubview:s];
        [s startAnimating];
    }
    return self;


}

- (id)initWithDelegate:(UIViewController*)viewController
{
    self = [self initWithFrame:viewController.view.frame];
    if (self)
    {
        self.delegate = viewController;
    }

    return self;
}


- (void)show
{
    [self.delegate.view addSubview:self];
}

- (void)hide
{
    [self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [_delegate release];
    [super dealloc];
}

@end
