#import "UUID.h"


@interface UUID()
@property(nonatomic, retain) NSData* data;
@end

@implementation UUID
{
    NSString* _stringValue;
}
@synthesize data = _data;

- (id)init
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFUUIDBytes rawData = CFUUIDGetUUIDBytes(uuid);
    NSData * nsData =  [NSData dataWithBytes:&rawData length:sizeof(CFUUIDBytes)];
    CFRelease((CFTypeRef) uuid);
    return [self initWithData:nsData];
}

- (id)initWithData:(NSData *)data
{
    if ([data length] != kUUIDLLength)
        @throw [NSException exceptionWithName:@"ArgumentException"
                                       reason:[NSString stringWithFormat:@"Invalid length for a UUID, must be 16 bytes but was %d", data.length]
                                     userInfo:nil];

    self = [super init];
    if (self)
    {
        self.data = data;
    }
    return self;
}

- (NSString *)stringValue
{
    if (_stringValue)
        return _stringValue;

    const unsigned char * buffer = [self.data bytes];
    NSUInteger length = [self.data length];
    NSMutableString *temp = [NSMutableString stringWithCapacity:length*2];
    for (int i = 0 ; i < length ; i++)
        [temp appendFormat:@"%02x", (unsigned int) buffer[i]];
    _stringValue = [NSString stringWithString:temp];
    return _stringValue;
}

- (NSString *)description
{
    return self.stringValue;
}


- (UUID *)copy
{
    NSData *dataClone = [self.data copy];
    UUID *copy = [[UUID alloc] initWithData:dataClone];
    return copy;
}

- (BOOL)isEqual:(id)object
{
    if (!object || ![object isKindOfClass:self.class])
        return NO;

    return [self isEqualToUUID:(UUID *)object];
}

- (BOOL)isEqualToUUID:(UUID *)other
{
    if (!other)
        return NO;
    if (other == self)
        return YES;

    return [_data isEqualToData:other.data];
}

- (NSUInteger)hash
{
    return [self.data hash];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.data = [aDecoder decodeDataObject];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDataObject:self.data];
}

+ (UUID *)fromString:(NSString *)string
{
    if (string.length != 2 * kUUIDLLength)
        return nil;

    unsigned char buffer[kUUIDLLength];
    NSRange range = NSMakeRange(0, 2);
    for(int i = 0 ; i < kUUIDLLength ; i++)
    {
        NSString * byteSubstring = [string substringWithRange:range];
        NSScanner * scanner = [NSScanner scannerWithString:byteSubstring];
        range.location += 2;
        unsigned int value = 0;
        if (![scanner scanHexInt:&value])
            return nil;

        buffer[i] = (unsigned char)value;
    }
    NSData * data = [NSData dataWithBytes:buffer length:kUUIDLLength];
    return [[UUID alloc] initWithData:data];
}

@end