//
//  DeviceHardware.h
//  Marktplaats
//
//  Created by Gyorgy Varadi on 1/20/11.
//  Copyright 2011 Marktplaats B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Reads and holds the device hardware information. Used to determine EXACT version of device hardware is running on.
 */
@interface DeviceHardware : NSObject {

}

/*!
 Read hardware string from the operating system
 @return platform hardware string
 */
- (NSString *)platform;

@end
