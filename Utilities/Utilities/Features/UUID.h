#import <Foundation/Foundation.h>

#define kUUIDLLength 16

@interface UUID : NSObject<NSCoding>

- (id)initWithData:(NSData *)data;
- (NSString *)stringValue;

+ (UUID *)fromString:(NSString *)string;

- (BOOL)isEqualToUUID:(UUID *)other;

@end