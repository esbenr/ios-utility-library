# iOS Utility Library	

This is a utility library with features I have found useful during my time as iOS developer.
In general I'm not found of the idea that you use a library and include a lot of code, just to use some of the features.
So I have tried to narrow the feature in this library down to a minimum being thin classes/extensions only in order to make it as lightweight as possible.

## Features

1. Bla. 
2. Bla.

## Categories

1. Bla
2. Bla
 
# How to use

Follow the 2nd part of the tutorial [here](http://www.blog.montgomerie.net/easy-xcode-static-library-subprojects-and-submodules).

# Requirements 

Some features support iOS 5.0+, but most should be compatible with iOS 4.3+  

# License

None for now, to be updated soon.
